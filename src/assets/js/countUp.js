var CountUpClass = function () {

    this.options = {
        useEasing : true,
        useGrouping : true,
        separator : ' ',
        decimal : ',',
        prefix : '',
        suffix : ''
    };



    this.init = function() {
        var countUps = $("[data-countUp]");
        if(!window.App) window.App = [];
        window.App.countUps = [];
        thisClass = this;
        if(!!countUps.length) {
            var countUpGroup = $("[data-countUp-group]");
            countUpGroup.each(function(k,v) {
                thisClass.isInScreen(window,v)
                $(window).scroll(function() {
                    if(thisClass.isInScreen(this,v))  {
                        thisClass.scroledTo(v);
                    }
                });
                var number = "",
                    end = 0,
                    prefix= "",
                    sufix = "",
                    untouched = 0,
                    decimal = 0,
                    options = {};
                $(v).find("[data-countUp]").each(function(k, up) {
                    options = thisClass.options;
                    number = String(up.dataset.countup);
                    untouched = number.match(/[0-9]+[0-9 ,.]*[0-9]+/)[0];
                    decimal = untouched.replace(",",".").split(".")[1];
                    if(!!decimal) {
                        decimal = decimal.length;
                    } else {
                        decimal = 0;
                    }

                    end = untouched.replace(" ","").replace(",",".")*1;
                    //console.log(end,number,decimal);
                    var splited = number.replace(untouched,"|").split("|");
                    prefix = !!splited[0]?splited[0]:prefix;
                    sufix = !!splited[1]?splited[1]:sufix;

                    options.prefix=prefix;
                    options.suffix=sufix;
                    $(up).empty();
                    window.App.countUps.push(new CountUp($(up).attr("id"), 0, end*1, decimal, 2.5, options));

                });
            });
        }
    };

    this.scroledTo = function(group) {
        window.App.countUps.forEach(function(item,key) {
            item.start();
        });
    };
    this.isInScreen = function(self,val) {
        var hT = $(val).offset().top,
            hH = $(val).outerHeight(),
            wH = $(window).height(),
            wS = $(self).scrollTop();

        if (wS > (hT+hH-wH)){
            return true
        } else {
            return false
        }
    };
    this.init();
};

$(function() {
    var cu = new CountUpClass();
    cu.scroledTo();
});