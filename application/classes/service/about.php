<?php defined('SYSPATH') or die('No direct script access.');


class Service_About extends Service_Page
{
    /**
     * Nacte stranku dle route_id
     * @param int $id
     * @return orm
     */
    public static function get_page_by_route_id($id)
    {
        $result_data = parent::get_page_by_route_id($id);

        $result_data["childs"] = array();
        $childs = orm::factory("page")
            ->join("routes")->on("route_id","=","routes.id")
            ->where("parent_id","=",$result_data["id"])
            ->where("routes.zobrazit","=",1)
            ->find_all();
        foreach ($childs as $child) {
            $result_data["childs"][$child->id] = self::get_page_by_route_id($child->route_id);
            $result_data["childs"][$child->id]["module_id"] = $child->route->module_id;
            $result_data["childs"][$child->id]["nazev_seo"] = $child->route->nazev_seo;
            if($child->route->module_id == 18) {
                $result_data["childs"][$child->id]["actions"] = Service_Action::get_actions(4);
            }
        }

        return $result_data;

    }

    /**
     * Načte timeline události
     * @param $language_id
     *
     * @return array
     */
    public static function get_timelines($language_id)
    {
        $result_data = array();
        $events = orm::factory("timeline")
            ->language($language_id)
            ->where("zobrazit","=",1)
            ->order_by("year","ASC")
            ->find_all();

        foreach ($events as $event) {
            $result_data[$event->id] = $event->as_array();
        }
        return $result_data;
    }
}