<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Places_Item_Edit extends Controller_Hana_Edit
{
	protected $with_route=false;

	public function before(){
		$this->orm = new Model_Place();
		parent::before();
	}

	protected function _column_definitions(){
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();

		$this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();

		$this->auto_edit_table->row("misto")->type("edit")->label("Název pobočky")->set();
		$this->auto_edit_table->row("funkce")->type("edit")->label("Účel pobočky")->set();

		$this->auto_edit_table->row("ulice")->type("edit")->label("Ulice a č.p")->set();
		$this->auto_edit_table->row("mesto")->type("edit")->label("Město")->set();
		$this->auto_edit_table->row("psc")->type("edit")->label("PSČ")->set();


		$this->auto_edit_table->row("tel")->type("edit")->label("Telefon")->set();
		$this->auto_edit_table->row("email")->type("edit")->label("Email")->set();
		$this->auto_edit_table->row("schranka")->type("edit")->label("Datová schránka")->set();




	}
}