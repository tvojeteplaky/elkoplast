<h2>{translate str="Kontaktujte nás"}</h2>
                    <div class="row">
                        <div class="large-3 medium-4 columns">
                            <p>
                                <span><img src="{$media_path}assets/img/tel.svg" alt="{translate str="Telefon"}"></span>{$owner.tel}
                                <br>
                                <span class="at">@</span>{$owner.email}
                            </p>
                        </div>
                        <div class="large-3 medium-3 columns">
                            <p>
                                {$owner.ulice}
                                <br>
                                {$owner.psc} {$owner.mesto}
                            </p>
                        </div>
                        <div class="large-6 medium-5 columns">
                            <p>
                                IČ:&nbsp;{$owner.ic}
                                <br>
                                DIČ:&nbsp;{$owner.dic}
                            </p>
                        </div>
                    </div>