{if !empty($items)}
<nav aria-label="You are here:" role="navigation">
	<ul class="breadcrumbs">
		<li><a href="{$url_homepage}">{translate str="Hlavní stránka"}</a></li>
		{foreach $items as $item} {if isset($item.nazev)} {if $item@last}
		<li>
			<span class="show-for-sr">Aktuální stránka: </span> {$item.nazev}
		</li>
		{else}
		<li><a href="{if isset($item.nazev_seo)}{$url_base}{$item.nazev_seo}{else}#{/if}">{$item.nazev}</a></li>
		{/if} {/if}
		{/foreach}
	</ul>
</nav>
{/if}
