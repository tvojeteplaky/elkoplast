<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Places_People_Edit extends Controller_Hana_Edit
{
	protected $with_route=false;

	public function before(){
		$this->orm = new Model_Place_People();
        $this->subject=strtolower($this->orm->class_name);
		parent::before();
	}

	protected function _column_definitions(){
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
		$this->auto_edit_table->row("jmeno")->type("edit")->label("Celé jméno")->set();
        $this->auto_edit_table->row("oddeleni")->type("edit")->label("Oddělení")->set();
        $this->auto_edit_table->row("funkce")->type("edit")->label("Funkce")->set();
 	/*if($this->orm->id && !$this->orm->place_id)
        {
            $this->auto_edit_table->row("place_id")->value($this->orm->place_id)->type("selectbox")->label("Pobočka")->data_src(array("related_table_1"=>"place","column_name"=>"misto","condition"=>array("zobrazit","=",1),"order_by"=>array("poradi","asc"),"orm_tree"=>false))->set();
        }
        else
        {
            $this->auto_edit_table->row("place_id")->type("selectbox")->label("Pobočka")->data_src(array("related_table_1"=>"place","column_name"=>"misto","condition"=>array("zobrazit","=",1),"order_by"=>array("poradi","asc"),"orm_tree"=>false))->set();
	  }*/
        $this->auto_edit_table->row("telefon")->type("edit")->label("Telefon")->set();
        $this->auto_edit_table->row("email")->type("edit")->label("Email")->set();

        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"jpg","delete_link"=>true))->label("Náhled obrázku")->set();
        $this->auto_edit_table->row("icon_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("icon_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true, "db_col_name"=>"icon_src"))->label("Náhled obrázku")->set();



        $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
        $this->auto_edit_table->row("product_category_id")->type("selectbox")->label("Svázané kategorie")->item_settings(array("HTML_array" =>array("style"=>"height:550px")))->data_src(array("related_table_1"=>"product_categories","column_name"=>"nazev","condition"=>array("zobrazit","=",1),"order_by"=>array("poradi","asc"),"orm_tree"=>true,"multiple"=>true))->set();
        $this->auto_edit_table->row("product_id")->type("selectbox")->label("Svázané produkty")->item_settings(array("HTML_array" =>array("style"=>"height:320px")))->data_src(array("related_table_1"=>"products","column_name"=>"nazev","condition"=>array("more","=",1),"order_by"=>array("poradi","asc"),"orm_tree"=>false,"multiple"=>true))->set();


    }

    protected function _form_action_main_postvalidate($data)
    {
        parent::_form_action_main_postvalidate($data);
        // ulozim k produktu reference na vybrane kategorie
        $this->module_service->bind_categories($data['product_category_id'], 'product_category', 'product_categories',false);
        $this->module_service->bind_categories($data['product_id'], 'product', 'products',false);

        if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
        {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($this->orm->jmeno));
        }
        if(isset($_FILES["icon_image_src"]) && $_FILES["icon_image_src"]["name"])
        {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "icon");
            $this->module_service->insert_image("icon_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($this->orm->jmeno)."-icon",true,"png","icon_src");
        }
    }
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir,false,false,false,"photo_src","ext",false);
    }
    protected function _form_action_icon_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir,false,false,false,"icon_src","ext",false);
    }

}