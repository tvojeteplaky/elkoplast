<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace produktu - edit.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2013 Pavel Herink
 */

class Controller_Admin_Cz_Catalog_Category_Edit extends Controller_Hana_Edit
{
    protected $with_route=true;
    protected $max_category_level=3;
    protected $item_name_property=array("nazev"=>"s názvem");

    public function before() {
        $this->orm=new Model_Catalog_Category();
        parent::before();
        $this->subject_dir=$this->module_key."/".$this->submodule_key."/";
        $this->action_buttons=array_merge($this->action_buttons,
            array("odeslat_do_detailu"=>array("name"=>"odeslat_do_detailu","value"=>"odeslat a otevřít detail"))

        );
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        $this->auto_edit_table->row("nazev_seo")->type("edit")->data_src(array("related_table_1"=>"route"))->label("Název SEO")->condition("(Pokud nebude položka vyplněna, vygeneruje se automaticky z názvu.)")->set();
        $this->auto_edit_table->row("nadpis")->type("edit")->label("Nadpis")->set();
        $this->auto_edit_table->row("title")->type("edit")->label("Titulek")->condition("(Pokud nebude položka vyplněna, použije se hodnota z názvu.)")->set();
        $this->auto_edit_table->row("description")->type("edit")->label("Popis")->set();
        $this->auto_edit_table->row("keywords")->type("edit")->label("Klíčová slova")->set();
        $this->auto_edit_table->row("global_poradi")->type("edit")->data_src(array("related_table_1"=>"catalog_order"))->label("Globální pořadí")->set();

         if($this->max_category_level>1)
        {
            $this->auto_edit_table->row("parent_id")->type("selectbox")->label("Nadřazená stránka")->item_settings(array("max_tree_level"=>$this->max_category_level))->data_src(array("column_name"=>"nazev","orm_tree"=>true,"null_row"=>"---","language"=>true))->set();
        }

        $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();

        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"jpg","delete_link"=>true))->label("Náhled obrázku")->set();

         $this->auto_edit_table->row("secondary_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("secondary_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true, "db_col_name"=>"icon_src"))->label("Náhled obrázku")->set();

        $this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();
        $this->auto_edit_table->row("uvodni_popis")->type("editor")->label("Popis pod produkty")->set();

    }

    protected function _form_action_main_prevalidate($data) {
        parent::_form_action_main_prevalidate($data);
        // specificka priprava dat, validace nedatabazovych zdroju (pripony obrazku apod.)
        if(!$data["title"] && $data["nazev"]){$data["title"]=$data["nazev"];}

        if(!$data["nazev_seo"] && $data["nazev"]){
            $data["nazev_seo"]=seo::uprav_fyzicky_nazev($data["nazev"]);
            $data["nazev_seo"]=$data["nazev_seo"];
        }elseif($data["nazev_seo"]){
            $data["nazev_seo"]=seo::uprav_fyzicky_nazev($data["nazev_seo"]);
        }
        if(!$data["nadpis"]) {
            $data["nadpis"] = $data["nazev"];
        }

        $data["module_id"]=orm::factory("module")->where("kod","=","catalog")->find()->id;

        if(!empty($data["parent_id"]))
        {
            $priorita = orm::factory("product_category")->select(array("product_categories.priorita","pcp"))->where("product_categories.id","=",$data["parent_id"])->find()->pcp;
            $data["priorita"]=$priorita+1;
            $data["module_action"]="category"; // specialni uvodka navazana na prvni uroven kategorii produktu
        }
        else
        {
            $data["priorita"]=0;
            $data["module_action"]="category";//"index";
        }


        $data["photo_src"] = $this->orm->photo_src;
        $data["icon_src"] = $this->orm->icon_src;

        return $data;
    }
     protected function _form_action_main_postvalidate($data) {
       parent::_form_action_main_postvalidate($data);


         // vlozim o obrazek
         if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
         {
             // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
             $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
             $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, $this->orm->route->nazev_seo, true, 'jpg',"photo_src");
         }
         if(isset($_FILES["secondary_image_src"]) && $_FILES["secondary_image_src"]["name"])
         {
             // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
             $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "icon");
             $this->module_service->insert_image("secondary_image_src", $this->subject_dir, $image_settings, "icon", true, 'png',"icon_src");
         }
         if($data["global_poradi"]) {
             if($this->orm->catalog_order->global_poradi) {
                 $catalog_order = $this->orm->catalog_order;
                 $catalog_order->global_poradi = $data["global_poradi"];
                 $catalog_order->save();
             } else {
                 $catalog_order = new Model_Catalog_Order();
                 $catalog_order->product_category_id = $this->orm->id;
                 $catalog_order->global_poradi = $data["global_poradi"];
                 $catalog_order->save();
             }
         }


    }

    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'jpg', false);
    }

    /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_secondary_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'icon_src', 'png', false);
    }


}