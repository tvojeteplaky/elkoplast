/**
 * Created by ondrej on 13.2.17.
 */
var ContactClass = function() {
    this.selectBoxes = ["OborSelectBox", "ProduktSelectBox"];
    this.defaultValues = { OborSelectBox: "Obor", ProduktSelectBox: "Produkt" };
    this.peopleBox = "peopleChangeable";
    this.buttonSend = "sendButton";


    this.insertDefaultValue = function(id) {
        var selectBox = document.getElementById(id);
        var option = this.createOption("",this.defaultValues[id]);
        selectBox.appendChild(option);
    }
    this.setupWatchers = function() {
        var parent = this;
        this.selectBoxes.forEach(function(item, index) {
            var selectBox = document.getElementById(item);
            if(!!selectBox) {
                selectBox.addEventListener("change", parent.selectBoxChanged);
            }
        });
        var button = document.getElementById(this.buttonSend);
        if(!!button) {
            button.addEventListener("click",parent.buttonClicked);
        }
    }

    this.getData = function(data,callback) {
        $.ajax({
            url: "",
            context: document.body,
            data: data,
            success: function(resp) {
                callback(JSON.parse(resp));
            },
            error: function() {
                return false;
            }
        });
    }

    this.cleanSelectBox = function(id) {
        var selectbox = document.getElementById(id);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }
    this.createOption = function(val,text) {
        var option = document.createElement("option");
        option.value = val;
        var t = document.createTextNode(text);
        option.appendChild(t);
        return option;
    }
    this.selectBoxChanged = function(event) {
        var contact = window.contactClass,
            data = {}
        selected = {};
        contact.selectBoxes.forEach(function(item,index) {
            var selectbox = document.getElementById(item);
            data[selectbox.name] = selectbox.value;
            selected[item] = selectbox.value;
        });
        contact.getData(data, function(resp) {
            if(resp instanceof Object) {
                var main = resp.main_content
                contact.selectBoxes.forEach(function(item,index) {
                    if(main[item] instanceof Object) {
                        contact.cleanSelectBox(item);
                        contact.insertDefaultValue(item);
                        $.each(main[item],function(k,v) {
                            var selectBox = document.getElementById(item);
                            var option = contact.createOption(v.id,v.nazev);
                            if(selected[item]*1 == v.id*1) {
                                option.selected = "selected";
                            }
                            selectBox.appendChild(option);
                        });
                    }
                });
            }
        });

    }
    this.buttonClicked = function(ev) {
        var contact = window.contactClass,
            data = {};
        contact.selectBoxes.forEach(function(item,index) {
            var selectbox = document.getElementById(item);
            data[selectbox.name] = selectbox.value;
        });
        contact.getData(data, function(resp) {
            if(resp instanceof Object) {
                var main = resp.main_content
                var people = document.getElementById(contact.peopleBox);
                people.innerHTML = main.people;
            }
        });
    }

    this.setupWatchers();

}
$(document).ready(function() {
    window.contactClass = new ContactClass();
});