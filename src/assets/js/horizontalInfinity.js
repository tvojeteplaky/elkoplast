/**
 * Created by libor on 08.10.2016.
 */


$(document).ready(function() {
    if ($(".horizontalInfinity").length) {
        $(".horizontalInfinity .viewfinder").each(function() {
            $(this).css("padding-top", $(this).children().height() + 2 + "px");
        });
    }

    var isDown = false;
    forward = $(".horizontalInfinity .forward");
    back = $(".horizontalInfinity .back");
    forward.mousedown(function(){isDown = true;});
    back.mousedown(function(){isDown = true;});
    $(document).mouseup(function(){
        if(isDown){
            isDown = false;
        }
    });

    forward.mousedown(function() {move('-=20');});
    back.mousedown(function() {move('+=20');});

    function move(intMovement){
        $( ".horizontalInfinity .picture" ).animate({
            'left': intMovement +'px'
        }, 5, function() {
            if (isDown){
                move(intMovement);
            }
        });
    }
});


// target elements with the "draggable" class
interact('.horizontalInfinity .picture')
    .draggable({
        // enable inertial throwing
        inertia: false,
        // keep the element within the area of it's parent
        restrict: {
            restriction: "parent",
            endOnly: true
        },
        // enable autoScroll
        autoScroll: true,

        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: function (event) {
            /*var textEl = event.target.querySelector('p');

             textEl && (textEl.textContent =
             'moved a distance of '
             + (Math.sqrt(event.dx * event.dx +
             event.dy * event.dy)|0) + 'px');*/
        }
    });

function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
        target.style.transform =
            'translate(' + x + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

interact('.horizontalInfinity .forward')
    .on('tap', function (event) {
        event.currentTarget.classList.toggle('switch-bg');
        event.preventDefault();
    })
    .on('doubletap', function (event) {
        event.currentTarget.classList.toggle('large');
        event.currentTarget.classList.remove('rotate');
        event.preventDefault();
    })
    .on('hold', function (event) {
        event.currentTarget.classList.toggle('rotate');
        event.currentTarget.classList.remove('large');
    });


/*
 setInterval(function() {
 $('.horizontalInfinity .forward').click(function() {
 var x = $('.horizontalInfinity .picture').css('transform').split(/[()]/)[1]
 $('.horizontalInfinity .picture').css("transform", "translate(" + x.split(',')[4] + "px)")
 });
 }, 20);
 */

