{if !empty($links)}
<ul class="accordion hide-for-large" data-accordion data-allow-all-closed="true">
    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">{translate str="Sekce"}</a>
        <div class="accordion-content" data-tab-content>
            <nav class="sideNav">
                <ul class="menu vertical">
                    {foreach from=$links item=link key=key name=name}
                    <li {if array_key_exists($link.nazev_seo, $sel_links)}class="active" {/if}><a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a></li>
                    {/foreach}
                </ul>
            </nav>
        </div>
    </li>
</ul>
<nav class="sideNav show-for-large">
    <ul class="menu vertical">
        {foreach from=$links item=link key=key name=name}
        <li {if array_key_exists($link.nazev_seo, $sel_links)}class="active" {/if}><a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a></li>
        {/foreach}
    </ul>
</nav>
{/if}
