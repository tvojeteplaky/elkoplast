<?php defined('SYSPATH') or die('No direct script access.');


class Model_Catalog_Order extends ORM
{

    protected $_belongs_to = array(
        'catalog' => array("foreign_key"=>"product_id"),
        'catalog_category' => array("foreign_key"=>"product_category_id"),
    );


}