{if !empty($messages)}
    <div class="reveal message" id="message" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
        {foreach $messages as $message}
            <p>{$message}</p>
        {/foreach}
    </div>
    <script type="text/javascript">
        document.messages = true;
    </script>
{/if}