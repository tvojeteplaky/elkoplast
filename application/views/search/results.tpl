
<section class="row15" id="search">
	<div class="large15-4 columns">
		{*<nav class="sideNav back">
			<ul class="menu vertical">
				<li class="active"><a href="#">{translate str="Zadat nové vyhledávání"}</a></li>
			</ul>
		</nav>*}
	</div>
	<div class="large15-11 columns ">
		<div class="row">
			<div class="small-12 columns">
				<header>
					<h1>{translate str="Výsledky vyhledávání"}</h1>
				</header>
				<p>{translate str="Veškeré výsledky pro hledaný výraz"} <strong>“{$keyword}”</strong></p>
			</div>
			<div class="items">
			{foreach from=$search_results item=items key=key name=name}
			{foreach from=$items item=result key=key name=name}
				<a href="{$url_base}{$result.nazev_seo}">
                    <div class="row item">
                        <div class="small-4 columns">
                        	{if !empty($result.photo)}
                        		<img src="{$result.photo}" alt="{$result.title}">
                        	{/if}
                        </div>
                        <div class="small-8 columns">
                            <h2>{$result.title}</h2>
                            <p>
								{$result.text|strip_tags:false|truncate:250:"...":true}
                            </p>
                        </div>
                    </div>
                </a>
                {/foreach}
			{/foreach}
			</div>
		</div>
	</section>