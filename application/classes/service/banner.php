<?php 

class Service_Banner
{

	public static $navigation_module="banner";

	protected static $thumb = "ad";
	public static $order_by="poradi";
	public static $order_direction="asc";

	public static $photos_resources_dir="/media/photos/";

	public static function get_list($language_id)
	{
		$result_data = array();
		$nodes = orm::factory('banner')
					->where('language_id',"=",$language_id)
					->where('zobrazit',"=",1)
					->order_by(self::$order_by,self::$order_direction)
					->find_all();

		foreach ($nodes as $node) {
			$result_data[$node->id] = $node->as_array();
			if($node->photo_src){
				$dirname=self::$photos_resources_dir.self::$navigation_module."/item/images-".$node->id."/".$node->photo_src."-".self::$thumb.".png";
				if(file_exists(str_replace('\\', '/', DOCROOT).$dirname)){

					$result_data[$node->id]["photo"] = $dirname;
				}
			}
		}
		return $result_data;
	}	
}