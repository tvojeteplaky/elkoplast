<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Places_People_List extends Controller_Hana_List
{
	protected $with_route=false;

   	protected $default_order_by = "poradi";
    	protected $default_order_direction= "asc";

       public function before() {
        $this->orm=new Model_Place_People();

        parent::before();
    }

        protected function _column_definitions()
   	{
       $this->auto_list_table->column("id")->label("# ID")->width(30)->set();
       $this->auto_list_table->column("jmeno")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(300)->set();
       $this->auto_list_table->column("place")->label("Pobočka")->data_src(array("related_table_1"=>"place", "column_name"=>"misto"))->css_class("txtLeft")->sequenceable()->filterable()->width(150)->set();


	 if(Kohana::config("languages")->get("enabled"))
        $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid"=>$this->base_path_to_edit))->width(58)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action"=>"change_visibility","states"=>array(0=>array("image"=>"lightbulb_off.png","label"=>"neaktivní"),1=>array("image"=>"lightbulb.png","label"=>"aktivní"))))->sequenceable(false)->label("")->width(32)->set();
        $this->auto_list_table->column("poradi")->type("changeOrderShifts")->label("")->sequenceable()->width(32)->exportable(false)->printable(false)->set();

        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();

      }
}