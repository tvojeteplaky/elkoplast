<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Action extends Controller
{
    public function action_detail() {
        $view = new View("page/detail");
        $view->item = Service_Action::get_action_by_route_id($this->application_context->get_actual_route());
        $view->no_subnav = true;
        $this->request->response = $view->render();
    }

}

