{if !empty($people)}
<div class="row small-up-1 large-up-3 spravnaosoba">
    {foreach $people as $person}
        <div class=" columns">
            <img src="{if !empty($person.photo_detail)}{$person.photo_detail}{else}{$media_path}assets/img/noimage.jpg{/if}" alt="{$person.jmeno}">
            <p><strong>{$person.jmeno}</strong>
                <br>
                {$person.funkce}
                <br>
                <br>
                 <strong>{$person.telefon}  •<br>  {$person.email}</strong></p>
        </div>
    {/foreach}
</div>
{/if}