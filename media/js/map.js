
function enableScrollingWithMouseWheel(map) {
	var el = $('#map');
	if (el.length > 0) {
		map.setOptions({
			scrollwheel: true
		});
	}
}

function disableScrollingWithMouseWheel(map) {
	var el = $('#map');
	if (el.length > 0 && map.length > 0) {
		map.setOptions({
			scrollwheel: false
		});
	}
}

function initMap() {
	var map2Selector = document.getElementById('map2');
    var image = '/media/assets/img/mapicon.png';
    if(!!map2Selector) {
        var markerMap2 = {lat: 49.2565274, lng: 17.5866088};

        var map2 = new google.maps.Map(map2Selector, {
            zoom: 12,
            center: markerMap2,
            scrollwheel: false
        });

        var marker = new google.maps.Marker({
            position: markerMap2,
            map: map2,
            icon: image
        });
    }

    var el = $('#map');
	if(!!$("#map").length ) {
		var markers = [
			{position:{lat:49.224219,lng:17.672593},title:"Centrála Zlín (správa, obchod)",id:"psc76001"},
			{position:{lat:49.266456,lng:17.760846},title:"Centrála Ostrata (sklolamináty)",id:"psc76311"},
			{position:{lat:49.992024,lng:17.474977},title:"Bruntál (kovovýroba, plasty)",id:"psc79201"}

		];
		var myLatLng = {lat:48.974148,lng:17.265602};
		var center = {lat:49.526732,lng:17.586643};
		map = new google.maps.Map(el[0], {
			zoom: 8,
			center: center,
			scrollwheel: false, // disableScrollingWithMouseWheel as default
			mapTypeId: google.maps.MapTypeId.ROADMAP

		});

		if (!!el.length) {
			google.maps.event.addListener(map, 'mousedown', function() {
				enableScrollingWithMouseWheel()
			});
		}
		markers.forEach(function(v,k) {
			var marker = new google.maps.Marker({
				position: v.position,
				map: map,
				title: v.title,
                icon: image
			});
			 marker.addListener('click', function() {
	          map.setZoom(13);
	          map.setCenter(marker.getPosition());
	        });
			document.getElementById(v.id).addEventListener("mouseover",function() {
				map.setZoom(13);
	          	map.setCenter(marker.getPosition());
			});
			document.getElementById(v.id).addEventListener("mouseout",function() {
				map.setZoom(8);
				map.setCenter(center);
			});
		})
		$(document).ready(function() {
			var el = $('#map');
			if(!!el) {
				$('body').on('mousedown', function(event) {
					var clickedInsideMap = $(event.target).parents('#map').length > 0;
					if (!clickedInsideMap) {
						disableScrollingWithMouseWheel(map);
					}
				});
				$(window).scroll(function() {
					disableScrollingWithMouseWheel(map);
				});
			}
		});
	}
}