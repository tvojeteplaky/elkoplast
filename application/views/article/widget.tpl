<section id="news">
        <div class="row">
            <div class="small-12 columns">
                <header class="headline">
                    <h2>{translate str="Co se děje <strong>nového</strong>"}</h2>
                </header>
            </div>
        </div>
        <div class="row large-up-3 paddingFix " id="newsList">
        {$items}
        </div>
        <div class="row15">
            <div class="large15-3 columns all15-centered">
                {*TODO - načítání po kliknutí*}
                <button class="expanded button" id="loadNext" data-url="{hana_secured_get action="widget" module="article"}" type="button">Načíst další</button>

            </div>
        </div>
    </section>