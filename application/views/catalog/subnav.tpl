<div class="large15-4 columns">
            <ul class="accordion hide-for-large" data-accordion data-allow-all-closed="true">
                <li class="accordion-item" data-accordion-item>
                    <a class="accordion-title">{translate str="Sekce"}</a>
                    <div class="accordion-content" data-tab-content>
                        <nav class="sideNav product">
                            <ul class="menu productSub vertical">
                            {foreach $links as $link}
                                    
                                <li {if array_key_exists($link.nazev_seo, $sel_links)}class="active"{/if}>
                                    <a href="{$url_base}{$link.nazev_seo}">
                                        {if !empty($link.icon_small)}
                                            <img src="{$link.icon_small}" alt="{$link.nazev}">
                                        {/if}
                                        {$link.nazev}
                                    </a>
                                    {if !empty($link.childs) && array_key_exists($link.nazev_seo, $sel_links)}
                                    
                                        <ul class="menu vertical nested">
                                            {foreach $link.childs as $linkL2}
                                                <li>
                                                    <a href="{$url_base}{$linkL2.nazev_seo}">{$linkL2.nazev}</a>
                                                    {if !empty($linkL2.childs) && !empty($linkL2.childs) && array_key_exists($linkL2.nazev_seo, $sel_links)}
                                    
                                                        <ul class="menu vertical nested">
                                                            {foreach $linkL2.childs as $linkL3}
                                                                <li>
                                                                    <a href="{$url_base}{$linkL3.nazev_seo}">{$linkL3.nazev}</a>
                                                                </li>
                                                            {/foreach}
                                                        </ul>
                                                    {/if}
                                                </li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                            </ul>
                        </nav>
                    </div>
                </li>
            </ul>
            <nav class="sideNav show-for-large product">
                <ul class="menu productSub vertical">
                    {foreach from=$links item=link key=key name=name}
                        <li {if array_key_exists($link.nazev_seo, $sel_links)}class="active"{/if}>
                            <a href="{$url_base}{$link.nazev_seo}">
                                {if !empty($link.icon_small)}
                                    <img src="{$link.icon_small}" alt="{$link.nazev}">
                                {/if}
                                {$link.nazev}
                            </a>
                            {if !empty($link.childs) && array_key_exists($link.nazev_seo, $sel_links)}

                                        <ul class="menu vertical nested">
                                            {foreach $link.childs as $linkL2}
                                                <li {if array_key_exists($linkL2.nazev_seo, $sel_links)}class="active"{/if}>
                                                    <a href="{$url_base}{$linkL2.nazev_seo}">{$linkL2.nazev}</a>
                                                    {if !empty($linkL2.childs) && array_key_exists($linkL2.nazev_seo, $sel_links)}

                                                        <ul class="menu vertical nested">
                                                            {foreach $linkL2.childs as $linkL3}
                                                                <li {if array_key_exists($linkL3.nazev_seo, $sel_links)}class="active"{/if}>
                                                                    <a href="{$url_base}{$linkL3.nazev_seo}">{$linkL3.nazev}</a>
                                                                </li>
                                                            {/foreach}
                                                        </ul>
                                                    {/if}
                                                </li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                        </li>
                    {/foreach}
                </ul>
            </nav>
        </div>