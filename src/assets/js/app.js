$(document).foundation();
$(window).load(function() {
	$("#loader").fadeOut("slow");
});
$(".title-bar .menu-icon").click(function(){
    $(window).scrollTop(0);
    var menu =  $("nav.sticky-container .sticky");
    menu.toggleClass("is-unstucked");
    $("#contentAfterMenu").toggleClass("hide");
});


window.sr = ScrollReveal();
var options = { duration: 700, distance: "40px"};
sr.reveal('.animate', options);

