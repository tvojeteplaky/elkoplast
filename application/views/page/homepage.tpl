<section id="slider">
        <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-auto-play="true" data-timer-delay="3000">
            <ul class="orbit-container">
                {foreach $item.photos as $photo}
                <li class="{if $photo@first}is-active{/if} orbit-slide">
                    <div>
                        <img src="{$photo.small}" alt="{$photo.nazev}">
                        <div class="row15">
                            <div class="large15-8 columns slideText ">
                                <h2><strong>{$photo.popis}</strong></h2>

                                <p><a href="{$photo.nazev}">Zjistěte více</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                {/foreach}
            </ul>
        </div>

    </section>
<style>
    #slider .orbit ul {
        margin-left: 0;
    }
    #slider .orbit li {
        padding: 0 !important;
    }
</style>

    {widget controller="catalog"}

    <section id="about">
        <div class="row">
            <div class="small-12 columns">
                <header class="headline">
                    <h2>{translate str="<strong>Zjistěte více</strong> o naší společnosti"}</h2>
                </header>
            </div>
        </div>
        <div class="row15 ">
            {static_content code="více-o-spolecnosti"}
        </div>
        {*<div class="row">
            <div class="small-12 columns ">
                <img src="{$media_path}assets/img/homeAboutPhoto.jpg" alt="Popelářská auta">
            </div>
        </div>*}
    </section>
    {widget controller="article"}

    <section id="whyUs">
        <div class="row15">
            <div class="medium15-5 columns">
                <img src="{$media_path}assets/img/cage.svg" alt="Kontejner">
                <strong>
                    {translate str="<b>Přední</b> Evropský
                    <br>
                    výrobce"}
                </strong>
            </div>
            <div class="medium15-5 columns">
                <img src="{$media_path}assets/img/best.svg" alt="Medaile">
                <strong>
                    {translate str="Držitel certifikátů
                    <br>
                    <b>ISO 9001, 14001</b>"}
                </strong>
            </div>
            <div class="medium15-5 columns">
                <img src="{$media_path}assets/img/truck.svg" alt="Kamion">
                <strong>
                    {translate str="<b>Celoevropská</b>
                    <br>
                    kamionová přeprava"}
                </strong>
            </div>
        </div>
    </section>

        {if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}{$item.id}" target="_blank" class="button">Editovat</a>
</div>
{/if}