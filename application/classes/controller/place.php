<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Place extends Controller
{
	public function action_main() {
		$language_id = $this->application_context->get_actual_language_id();

		$view = new View("place/main");
		$view->places = Service_Place::get_places($language_id, 3);
		$this->request->response = $view->render();
	}

	public function action_people($nazev_seo,$obor_id = false) {
		$language_id = $this->application_context->get_actual_language_id();
		$view = new View("place/people");
       $view->people = Service_Place::get_people($language_id,false,$obor_id,false);
		$this->request->response = $view->render();
	}

	public function action_category_people($nazev_seo,$cat_id=false) {
		$language_id = $this->application_context->get_actual_language_id();
		$view = new View("place/cat_people");
		$view->people = Service_Place::get_people($language_id,$cat_id,false,false);
		$this->request->response = $view->render();
	}
	public function action_product_people($nazev_seo,$prod_id=false) {
		$language_id = $this->application_context->get_actual_language_id();
		$view = new View("place/cat_people");
		$view->people = Service_Place::just_people($language_id,$prod_id);
		$this->request->response = $view->render();
	}
}