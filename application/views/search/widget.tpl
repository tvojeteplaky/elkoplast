<a href="#"><img src="{$media_path}assets/img/search.svg" alt="Hledat"></a>
<ul class="submenu menu vertical">
                                    <li>
                                        <form method="post" action="{translate str="vysledky-vyhledavani"}">
                                        <div class="input-group">
                                            <input class="input-group-field" name="search_text" type="text" id="search" placeholder="Zadejte hledaný výraz" data-url="{hana_secured_get action="show_suggestions" module="search"}">
                                            <div class="input-group-button">
                                                <button class="button"></button>
                                            </div>
                                        </div>
                                        {hana_secured_post action="index" module="search"}
                                        </form>
                                    </li>
                                </ul>