<!--Horní navigace-->
    <nav data-sticky-container="">
      <div class="sticky" data-sticky data-options="anchor: page; marginTop: 0; stickyOn: small;">
        <!--Otevírací panel na mobilech-->
        <div class="title-bar" data-responsive-toggle="mainNav" data-hide-for="large">
        	<a href="/"><img src="{$media_path}assets/img/logo.svg" alt="Elkoplast logo" style="width:9.5rem"></a>
        	<div style="position: absolute;right: 0;top: 0.6rem;">

	          <div class="title-bar-title">{translate str="Menu"}</div>
	          <button class="menu-icon" type="button" data-toggle></button>
        	</div>
        </div>
        <!--Horní panel-->
        <div class="tob-bar" id="mainNav">
          <div class="row15">
            <!--Levá navigace-->
            <div class="large15-4 columns">
              <ul class="menu">
                <li class="menu-text"><a href="/"><img src="{$media_path}assets/img/logo.svg" alt="Elkoplast logo" style="width:13.5rem"></a></li>
              </ul>
            </div>
            <!--Pravá navigace-->
            <div class="large15-11 columns">
              <!--Horizontální dropdown na desktopu, vertikální accordion na mobilech-->
              <ul class="dropdown vertical large-horizontal menu paddingFix" data-responsive-menu="accordion large-dropdown">
                  {foreach from=$links item=linkL1 key=key name=name}
                    {if $linkL1.indexpage}
                      <li><a href="/"><img src="{$media_path}assets/img/home.svg" alt="Domů"></a></li>
                    {elseif $linkL1.module_id == 6}
                       <li class="has-submenu products {if array_key_exists($linkL1.nazev_seo, $sel_links)}active{/if}">
                        {foreach from=$linkL1.children item=linkL2 key=key name=navL2}
                                   {if $smarty.foreach.navL2.first}
                                   <a href="{$url_base}{$linkL2.nazev_seo}">Produkty</a>
                                   <ul class="submenu menu vertical">
                                       <li>
                                           <div class="row15 large-up-5">
                                   {/if}
                          {if $smarty.foreach.navL2.index<5}


                           <div class="column">
                                <a href="{$url_base}{$linkL2.nazev_seo}">
                                  <div>
                                    <img src="{$linkL2.icon}" alt="{$linkL2.nazev}">
                                    <h2>{$linkL2.nazev}</h2>
                                  </div>
                                </a>
                              </div>
                               {/if}
                        {/foreach}
                            </div>
                          </li>
                        </ul>
                      </li>
                    {else}


                      <li class="{if array_key_exists($linkL1.nazev_seo, $sel_links)}active{/if}{if !empty($linkL1.children)} has-submenu pages{/if}">
                        <a href="{$url_base}{$linkL1.nazev_seo}" {if $linkL1.module_id == 2}class="button"{/if}>
                          
                            {$linkL1.nazev}
                         

                        </a>
                        {if !empty($linkL1.children)} 
                         <ul class="submenu menu  vertical">
                          {foreach $linkL1.children as $linkL2}
                            <li><a href="{$url_base}{if $linkL1.id == 5}{$linkL1.nazev_seo}#{/if}{$linkL2.nazev_seo}" {if $linkL1.id == 5}data-seo="{$linkL2.nazev_seo}{/if}">{$linkL2.nazev}</a></li>
                          {/foreach}
                         </ul>
                        {/if}
                      </li>
                    {/if}
                  {/foreach}

                <li class="has-submenu language">
                  {widget controller="site" action="languagebox"}
                </li>
                <li class="has-submenu search">
                  {widget controller="search"}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>