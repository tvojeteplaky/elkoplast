<form action="#" class="dotaz " method="post">
                <div class="row">
                    <div class="small-12 columns">
                        <h2>{translate str="Máte nějaký dotaz ohledně produktu?"}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="text" name="contactform[jmeno]" placeholder="{translate str="Jméno a příjmení"}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="tel" name="contactform[telefon]" placeholder="{translate str="Telefonní číslo"}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="email" name="contactform[email]" placeholder="{translate str="Emailová adresa"}">
                            </div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-12 columns">
                                <textarea name="contactform[zprava]" placeholder="{translate str="Text Vaší zprávy"}"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns text-center">
                        <button type="submit" class="button">
                            {translate str="Odeslat zprávu"}
                        </button>
                    </div>
                </div>
                {hana_secured_post action="send" module="contact"}
            </form>