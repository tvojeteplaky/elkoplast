<?php defined('SYSPATH') or die('No direct script access.');


class Service_Action extends Service_Page
{
    public static function get_action_by_route_id($route_id)
    {
        $action = orm::factory("action")
            ->join("routes")->on("route_id","=","routes.id")
            ->where("action_data.route_id","=",$route_id)
            ->where("routes.zobrazit","=",1)
            ->find();

        return  self::get_action($action);
    }

    public static function get_actions($limit = 1000)
    {
        $result_data = array();

        $actions = orm::factory("fair")
            ->where("zobrazit","=",1)
            ->limit($limit)
            ->order_by("poradi","ASC")
            ->find_all();
        foreach ($actions as $action) {
            $result_data[$action->id] = self::get_action($action);
        }
        return $result_data;
    }

    public static function get_action(Model_Fair $action)
    {
        $result_data = $action->as_array();
        $dirname=self::$photos_resources_dir."fair/item/images-".$action->id."/";

        if($action->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$action->photo_src."-list.jpg"))
        {
            $result_data["photo"]=url::base().$dirname.$action->photo_src."-list.jpg";
        } else {
            $result_data["photo"]="";
        }
        return $result_data;
    }
}