$(function() {
    $("#search").autocomplete({
        source: function(request, response) {
            jQuery.ajax({
                url: "?"+$("#search").data("url"),
                type: "get",
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    response(jQuery.map(data.main_content, function(item) {
                        return {
                            url: item.url,
                            value: item.name
                        }
                    }))
                }
            })
        },
        select: function( event, ui ) {
            window.location.href = ui.item.url;
        },
        minLength: 2
    });

});
