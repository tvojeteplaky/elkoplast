<h2>{translate str="Kam pokračovat dál"}</h2>
<nav class="row">
    <div class="medium-3 large-7 columns">
        <ul class="menu  vertical">
    {foreach from=$links item=link key=key name=site_index}
        {if !$smarty.foreach.site_index.first && $smarty.foreach.site_index.index%4 == 0}
            <div class="medium-9 large-5 columns">
                <ul class="menu vertical">
        {/if}
            <li><a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a></li>
        {if !$smarty.foreach.site_index.last && $smarty.foreach.site_index.iteration%4 == 0}
                </ul>
            </div>
        {/if}
    {/foreach}
        </ul>
    </div>
</nav>