<?php defined('SYSPATH') or die('No direct script access.');


class Controller_About extends Controller
{
    public function action_index() {
        $route_id=$this->application_context->get_actual_route();
        $view = new View("about/index");
        $view->item = Service_About::get_page_by_route_id($route_id);
        $view->events = Service_About::get_timelines($this->application_context->get_actual_language_id());
        if(Auth::instance()->logged_in(array('login', 'admin'))) {
            $view->edit = url::base()."admin/cz/page/item/edit/".$this->application_context->get_actual_language_id()."/";
        }
        $this->request->response = $view->render();
    }

}

