<section id="services">
        <div class="row">
            <div class="small-12 columns">
                <header class="headline">
                <h2>{translate str="Přehled našich produktů"}</h2>{* Prohlédněte si <strong>naše služby</strong> *}
                </header>
            </div>
        </div>
        <div class="row medium-up-2 large-up-3 paddingFix ">
        {foreach from=$categories item=cat key=key name=name}
            <div class="column">
                <a href="{$url_base}{$cat.nazev_seo}">
                    <div class="service {$colors[$cat.id]}">
                        <img src="{$cat.photo}" alt="{$cat.nazev}">
                        <div>
                            <strong>{$cat.nazev}</strong>
                        </div>
                    </div>
                </a>
            </div>
        {/foreach}
        </div>
</section>
