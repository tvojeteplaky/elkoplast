<div class="row peopleWidget">
	<div class="columns">
	{foreach $people as $person}
		<div class="row">
			<div class="medium-2 columns">
				<img src="{$person.icon}">
			</div>
			<div class="medium-5 columns">
				<p>{$person.jmeno}<br>Kontaktujte našeho obchodníka!</p>
			</div>
			<div class="medium-5 columns right">
				<p>{$person.telefon}<br><a href="mailto:{$person.email}">{$person.email}</a></p>
			</div>
		</div>
	{/foreach}


	</div>
</div>
