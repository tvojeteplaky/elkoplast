<?php defined('SYSPATH') or die('No direct script access.');

class Model_Place_People extends ORM_Language
{

	protected $_join_on_routes=false;

	// Validation rules
    protected $_rules = array(
        'jmeno' => array(
            'not_empty'  => NULL,
        ),
        'email' => array(
            'email'  => NULL,
        ),
    );

    protected $_belongs_to = array(
   	"place" => array(),
   	);
    protected $_has_many = array(
        "product_categories" => array("through"=>"place_peoples_product_categories"),
        "products" => array("through"=>"place_peoples_products"),
    );
}