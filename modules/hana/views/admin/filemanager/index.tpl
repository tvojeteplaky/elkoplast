<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>elFinder 2.0</title>

    <!-- jQuery and jQuery UI (REQUIRED) -->
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

    <!-- elFinder CSS (REQUIRED) -->
    <link rel="stylesheet" type="text/css" media="screen" href="{$media_path}admin/js/elfinder/css/elfinder.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="{$media_path}admin/js/elfinder/css/theme.css">

    <!-- elFinder JS (REQUIRED) -->
    <script type="text/javascript" src="{$media_path}admin/js/elfinder/js/elfinder.min.js"></script>

    <!-- elFinder translation (OPTIONAL) -->
    <script type="text/javascript" src="{$media_path}admin/js/elfinder/js/i18n/elfinder.cs.js"></script>

    <!-- elFinder initialization (REQUIRED) -->
    <script type="text/javascript" charset="utf-8">
        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
            var match = window.location.search.match(reParam) ;

            return (match && match.length > 1) ? match[1] : '' ;
        }

        $().ready(function() {
            var funcNum = getUrlParam('CKEditorFuncNum');

            var elf = $('#elfinder').elfinder({
                url : '/admin/file_manager',
                lang: 'cs',
                getFileCallback : function(file) {
                    window.opener.CKEDITOR.tools.callFunction(funcNum, file);
                    window.close();
                },
                resizable: false
            }).elfinder('instance');
        });
    </script>
</head>
<body>

<!-- Element where elFinder will be created (REQUIRED) -->
<div id="elfinder"></div>

</body>
</html>


{* <!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>File Manager</title>
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/styles/reset.css" />
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/scripts/jquery.filetree/jqueryFileTree.css" />
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/scripts/jquery.contextmenu/jquery.contextMenu-1.01.css" />
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/styles/filemanager.css" />
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/styles/ie9.css" />
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="{$media_path}admin/js/filemanager/styles/ie8.css" />
    <![endif]-->
</head>
<body>
<div>
    <form id="uploader" method="post">
        <button id="home" name="home" type="button" value="Home">&nbsp;</button>
        <h1></h1>
        <div id="uploadresponse"></div>
        <input id="mode" name="mode" type="hidden" value="add" />
        <input id="currentpath" name="currentpath" type="hidden" />
        <div id="file-input-container">
            <div id="alt-fileinput">
                <input id="filepath" name="filepath" type="text" /><button id="browse" name="browse" type="button" value="Browse"></button>
            </div>
            <input	id="newfile" name="newfile" type="file" />
        </div>
        <button id="upload" name="upload" type="submit" value="Upload"></button>
        <button id="newfolder" name="newfolder" type="button" value="New Folder"></button>
        <button id="grid" class="ON" type="button">&nbsp;</button>
        <button id="list" type="button">&nbsp;</button>
    </form>
    <div id="splitter">
        <div id="filetree"></div>
        <div id="fileinfo">
            <h1></h1>
        </div>
    </div>
    <form name="search" id="search" method="get">
        <div>
            <input type="text" value="" name="q" id="q" />
            <a id="reset" href="#" class="q-reset"></a>
            <span class="q-inactive"></span>
        </div>
    </form>

    <ul id="itemOptions" class="contextMenu">
        <li class="select"><a href="#select"></a></li>
        <li class="download"><a href="#download"></a></li>
        <li class="rename"><a href="#rename"></a></li>
        <li class="move"><a href="#move"></a></li>
        <li class="replace"><a href="#replace"></a></li>
        <li class="delete separator"><a href="#delete"></a></li>
    </ul>

    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.form-3.24.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.splitter/jquery.splitter-1.5.1.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.filetree/jqueryFileTree.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.contextmenu/jquery.contextMenu-1.01.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.impromptu-3.2.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/jquery.tablesorter-2.7.2.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/filemanager/scripts/filemanager.js"></script>
</div>
</body>
</html> *}