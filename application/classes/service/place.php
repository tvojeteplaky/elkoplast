<?php defined('SYSPATH') or die('No direct script access.');

class Service_Place
{
	protected static $photos_resources_dir = "media/photos/";
	public static function get_places($language_id, $limit=3) {
		$result_data = array();
		$place_orm = orm::factory("place")
			->where("zobrazit","=",1)
			->language($language_id)
			->order_by("poradi");
			if($limit>0) {
				$place_orm =$place_orm->limit($limit);
			}


		foreach ($place_orm->find_all() as $place) {
			$result_data[$place->id] = $place->as_array();
		}

		return $result_data;
	}

	public static function get_all_places_with_people($language_id) {
		$result_data = array();
		$place_orm = orm::factory("place")
			->where("zobrazit","=",1)
			->language($language_id)
			->order_by("poradi")
			->find_all();

		foreach ($place_orm as $place) {
			$result_data[$place->id] = $place->as_array();
			$result_data[$place->id]["people"] = array();
			$people_orm = $place->place_people->where("zobrazit","=",1)->language($language_id)->find_all();
			foreach ($people_orm as $person) {
				$result_data[$place->id]["people"][$person->id] = $person->as_array();
			}
		}
		return $result_data;
	}

	public static function get_people($language_id,$obor_id = false, $product_id = false, $limit = 3) {
		$result_data = array();
		$peoples = orm::factory("place_people")
			->where("zobrazit","=",1)
			->language($language_id);
		if($obor_id || $product_id) {
			$peoples->join("place_peoples_product_categories","LEFT")->on("place_peoples.id","=","place_peoples_product_categories.place_people_id");
			if((bool) $product_id*1) {
				$cat_ids = array();
				$product_orm = orm::factory("catalog")->where("more","=",1)->where("products.id","=",$product_id)->find();
				$cat_orm = orm::factory("catalog_category",$product_id);
				if($product_orm->loaded()) {
					$cat_orm = $product_orm->product_category->find();
					$cat_ids[] = $cat_orm->id;

				} elseif($cat_orm->loaded()) {
					$cat_ids[] = $cat_orm->id;

				}
				while ($cat_orm->loaded() && $cat_orm->parent_id*1>0) {
					$cat_orm = orm::factory("catalog_category",$cat_orm->parent_id);
					$cat_ids[] = $cat_orm->id;
				}
				

				$peoples->join("place_peoples_products","LEFT")->on("place_peoples.id","=","place_peoples_products.place_people_id");
				$peoples->and_where_open()
					->where("place_peoples_products.product_id","=",$product_id);
					if(!empty($cat_ids)) {
						$peoples->or_where("place_peoples_product_categories.product_category_id","IN",$cat_ids);
					}
					$peoples->and_where_close();

			} elseif((bool) $obor_id*1) {
				$cat_ids = array();
				$cat_orm = orm::factory("catalog_category",$obor_id);
		
				if($cat_orm->loaded()) {
					$cat_ids[] = $cat_orm->id;

				}
				while ($cat_orm->loaded() && $cat_orm->parent_id*1>0) {
					$cat_orm = orm::factory("catalog_category",$cat_orm->parent_id);
					$cat_ids[] = $cat_orm->id;
				}
				if(!empty($cat_ids)) {
					$peoples->where("place_peoples_product_categories.product_category_id","IN",$cat_ids);
				}
			}
		}
		if($limit) {
			$peoples->limit($limit);
		}
		$peoples =	$peoples
			->group_by("place_peoples.id")
			->order_by("poradi")
			->find_all();

		foreach ($peoples as $person) {
			$result_data[$person->id] = $person->as_array();
			$dirname=self::$photos_resources_dir."places/people/images-".$person->id."/";

            if($person->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$person->photo_src."-t1.jpg"))
            {
                $result_data[$person->id]["photo_detail"]=url::base().$dirname.$person->photo_src."-t1.jpg";
            } else {
            	$result_data[$person->id]["photo_detail"] = "";
            }

            if($person->icon_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$person->icon_src."-t1.png"))
            {
                $result_data[$person->id]["icon"]=url::base().$dirname.$person->icon_src."-t1.png";
            } else {
            	$result_data[$person->id]["icon"] = "";
            }
		}
		//die(var_dump($peoples));
		return $result_data;

	}

	public static function just_people($language_id, $product_id = false) {
		$result_data = array();
		$peoples = orm::factory("place_people")->where("zobrazit","=",1)
			->language($language_id);

		$peoples->join("place_peoples_product_categories")->on("place_peoples.id","=","place_peoples_product_categories.place_people_id");
			if((bool) $product_id*1) {
				$cat_ids = array();
				$product_orm = orm::factory("catalog")->where("products.id","=",$product_id)->find();
			}
			$cat_orm = orm::factory("catalog_category")->where("product_categories.id","=",$product_orm->product_category_id)->find();
			$cat_ids[] = $cat_orm->id;
			while ($cat_orm->loaded() && $cat_orm->parent_id*1>0) {
					$cat_orm = orm::factory("catalog_category",$cat_orm->parent_id);
					$cat_ids[] = $cat_orm->id;
				}


				$peoples->join("place_peoples_products")->on("place_peoples.id","=","place_peoples_products.place_people_id");
				$peoples->and_where_open();
					if($product_orm->more) {
						$peoples->where("place_peoples_products.product_id","=",$product_id);
					}
					if(!empty($cat_ids)) {
						$peoples->or_where("place_peoples_product_categories.product_category_id","IN",$cat_ids);
					}
					$peoples->and_where_close();

		$peoples =	$peoples
			->group_by("place_peoples.id")
			->order_by("poradi")
			->find_all();
		//die(var_dump($peoples));

		foreach ($peoples as $person) {
			$result_data[$person->id] = $person->as_array();
			$dirname=self::$photos_resources_dir."places/people/images-".$person->id."/";

            if($person->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$person->photo_src."-t1.jpg"))
            {
                $result_data[$person->id]["photo_detail"]=url::base().$dirname.$person->photo_src."-t1.jpg";
            } else {
            	$result_data[$person->id]["photo_detail"] = "";
            }

            if($person->icon_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$person->icon_src."-t1.png"))
            {
                $result_data[$person->id]["icon"]=url::base().$dirname.$person->icon_src."-t1.png";
            } else {
            	$result_data[$person->id]["icon"] = "";
            }
		}
		//die(var_dump($peoples));
		return $result_data;
	}
}