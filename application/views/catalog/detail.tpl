<div class="row">
    <div class="small-12 columns">
        {widget controller="navigation" action="breadcrumbs"}
    </div>
</div>
<section class="row15" id="products">
    {widget controller="catalog" action="subnav"}
    <div class="large15-11 columns">
        <div class="row">
            <div class="{if !empty($item.eshop_url)}medium-8{else}medium-12{/if} columns">
                <header>
                    <h1>{$item.nadpis}</h1>
                </header>
            </div>
            {if !empty($item.eshop_url)}
            <div class="medium-4 columns text-right">
                {* {if !empty($item.parent_seo)}<a href="{$url_base}{$item.parent_seo}" class="button buy tiny">&laquo; {translate str="Zpět do kategorie"}</a>{/if} *}<a href="{$item.eshop_url}"><img src="{$media_path}assets/img/tlacitko_eshop.png"></a>
            </div>
            {/if}
        </div>
        <div class="row">
            <div class="small-12 columns ">
                {if isset($main_photo)}
                <a href="{$main_photo.t2}" data-lightbox="gallery" data-title="{$main_photo.popis}"><img src="{$main_photo.t1}" alt="{$main_photo.nazev}" style="float: left;margin-right: 2rem; margin-bottom: 1rem;"></a>
                {/if}
                {$item.popis}
                
            </div>
        </div>
        {if !empty($item.photos)}
        <div class="row">
            <div class="small-12 columns">
                <h3>{translate str="Fotogalerie"}</h3>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns ">
                <section class="gallery">
                    <div class="row large-up-3 medium-up-2 small-up-1 productList {$color}">
                        {foreach from=$item.photos item=photo key=key name=name}
                        <div class="column">
                            <a href="{$photo.t2}" data-lightbox="gallery" data-title="{$photo.popis}">
                                <div class="product">
                                    <div>
                                        <img src="{if !empty($photo.t3)}{$photo.t3}{else}{$photo.t1}{/if}" alt="{if !empty($photo.nazev) && $photo.nazev != $photo.photo_src}{$photo.nazev} {/if}photo">
                                    </div>
                                    {if !empty($photo.nazev) && $photo.nazev != $photo.photo_src}
                                    <div class="title">
                                    <strong>{$photo.nazev}</strong>
                                    </div>
                                    {/if}
                                </div>
                            </a>
                        </div>
                        {/foreach}
                    </div>
                </section>
            </div>
        </div>
        {/if}
        <div class="row">
            <div class="small-12 columns ">
                {$item.uvodni_popis}
            </div>
        </div>
        {if !empty($item["files"])}
        <div class="row">
        <h2>{translate str="Ke stažení"}:</h2>
            {foreach $item["files"] as $file}
        <div class="small-12 large-4 columns {if $file@last}end{/if}" style="margin-bottom: 2rem;">
            <a href="{$file.file}" target="_blank">
                <div class="row">
                    <div class="small-3 columns">
                        <img src="{$file.file_thumb}" alt="{$file.nazev}">
                    </div>
                    <div class="small-9 columns">
                        <h4 style="margin-bottom: 0; color: #000; font-weight: 500;">{$file.nazev}</h4>
                        <a href="{$file.file}" download="download" target="_blank">{translate str="Stáhnout"}</a>
                    </div>
                </div>
            </a>
        </div>
            {/foreach}

    </div>
        {/if}
                {widget controller="place" action="product_people" param=$item.id}

        {widget controller="contact" action="show"}
    </div>
</section>
{if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}{$item.id}" target="_blank" class="button">Editovat</a>
</div>
{/if}