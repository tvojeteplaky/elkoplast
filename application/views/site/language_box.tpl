{*<a href="#">CZ</a>
<ul class="submenu menu vertical">
	<li><a href="#">CZ</a></li>
	<li><a href="#">EN</a></li>
</ul>*}
<a href="#">{$language_code|upper}</a>
<ul class="submenu menu vertical">
{foreach from=$languages item=lang_route key=lang name=languages_select}
	<li><a href="{$url_base}{$lang_route}">{$lang|upper}</a></li>
{/foreach}
</ul>