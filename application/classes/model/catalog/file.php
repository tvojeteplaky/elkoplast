<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Catalog_File extends ORM_Language {
	protected $_join_on_routes=false;
	public $_class_name="Product_File";

	protected $_belongs_to = array(
		'product' => array(),
	);
}