{*
CMS system Hana ver. 2.6 (C) Pavel Herink 2012
zakladni spolecna sablona layoutu stranek

automaticky generovane promenne spolecne pro vsechny sablony:
-------------------------------------------------------------
    $url_base      - zakladni url
    $tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
    $url_actual    - autualni url
$url_homepage  -  {$url_actual}cesta k homepage
    $media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
$controller
$controller_action
$language_code - kod aktualniho jazyku
$is_indexpage  - priznak, zda jsme na indexove strance

    $web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
    $web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
-------------------------------------------------------------

doplnujici custom Smarty funkce
-------------------------------------------------------------
                                    {translate str="nazev"}                                      - prelozeni retezce
                    {static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
    {widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
        {hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
{hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
{$product.cena|currency:$language_code}

Promenne do base_template:
-------------------------------------------------------------
{$page_description}
{$page_keywords}
{$page_name} - {$page_title}
{$main_content}
{include file="`$tpl_dir`dg_footer.tpl"}
{if !empty($web_owner.ga_script)}
    {$web_owner.ga_script}
    {/if}


*}

<!doctype html>
<html class="no-js" lang="cs">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{$page_title} - {$page_name}</title>
        <link rel="stylesheet" href="{$media_path}assets/css/app.css">
        {*<link rel="stylesheet" href="{$media_path}js/jquery-ui.min.css">
        <link rel="stylesheet" href="{$media_path}assets/css/lightbox.min.css">*}
    </head>
    <body>
   {* <div id="loader"><img src="{$media_path}assets/img/logo.svg" alt="Elkoplast" style="width: 13.5rem">

          <div class="cs-loader-inner">
            <label> ●</label>
            <label> ●</label>
            <label> ●</label>
            <label> ●</label>
            <label> ●</label>
            <label> ●</label>
          </div>

    </div>*}
    {widget controller="site" action="message"}
        <div id="page">
            {widget controller="navigation" action="main"}


        <div id="contentAfterMenu" class='{if $url_actual == "/index"}home{/if}'>
            {$main_content}

        </div>


         <footer>
            <div class="row15">
                <div class="large15-4 columns">
                    {widget controller="navigation" action="site_index"}
                </div>
                <div class="large15-11 columns">
                {widget controller="contact" action="footer"}

                </div>
            </div>
            <div class="row">
                <div class="small-12 columns text-center">
                    <!--logo-->
                </div>
            </div>
        </footer>
        </div>
        <script src="{$media_path}assets/js/app.js"></script>
       {* <script src="{$media_path}assets/js/lightbox.min.js"></script>
        <script src="{$media_path}js/jquery-ui.min.js"></script>
        <script src="{$media_path}js/search.js"></script>
        <script src="{$media_path}js/article.js"></script>
        <script src="{$media_path}js/tabs.js"></script>
        <script src="{$media_path}js/map.js"></script>
        <script src="{$media_path}js/contact.js"></script>*}
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrfCYcBaHd2iSA1bkS9saTRKtOw2LKpAk&amp;callback=initMap" async defer></script>
        <script type="text/javascript">
            $(window).load(function() {
                var hashname = window.location.hash.replace('#', '');
                var elem = $(window.location.hash);

                if(!!hashname.length && !!elem.length) {

                    $("html, body").animate({ scrollTop: elem.offset().top - 200 }, 600);
                }
                $("a[href*=#][data-open]").click(function(e) {
                    e.preventDefault();
                });
                $("a[href*=#][data-seo]").click(function(e) {
                    if(!!$("#" + $(this).data("seo")).length){
                        console.log($(this).data("seo"));
                        e.preventDefault();
                         $("html, body").animate({ scrollTop: $("#" +$(this).data("seo")).offset().top - 200 }, 600);
                    }
                });
            });  
        </script>
    </body>
</html>