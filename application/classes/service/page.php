<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * Servisa pro obsluhu statickych stranek.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Page extends Service_Hana_Page
{
    
    
    public static function get_pages_with_parent($language_id,$parent_id,$limit = 3)
    {
        $result_data = array();

        $nodes = orm::factory("page")
                    ->join('routes')->on("page_data.route_id","=","routes.id")
                    ->where("parent_id","=",$parent_id)
                    ->where("language_id","=",$language_id)
                    ->limit($limit)
                    ->find_all();

        foreach($nodes as $node){
            $result_data[$node->id] = $node->as_array();
            $result_data[$node->id]['nazev_seo'] = $node->nazev_seo;
            $dirname = self::$photos_resources_dir . self::$navigation_module. '/item/images-' . $node->id.'/';
            
            if($node->icon_src && file_exists( str_replace('\\', '/', DOCROOT) . $dirname . $node->icon_src . '-t1.png')){
                $result_data[$node->id]['icon'] =url::base(). $dirname . $node->icon_src . '-t1.png';
            } else {
                $result_data[$node->id]['icon'] = "";
            }
        }

        return $result_data;
    }
    /**
     * Vrati jeden segment retezce drobitkove navigace dle seo_nazvu.
     * @param type $nazev_seo
     * @return array
     */
    public static function get_navigation_breadcrumb($nazev_seo)
    {
        $return = array();
        $result_data = DB::select("page_data.nazev", "routes.nazev_seo", "routes.language_id");
        $result_data->select("pages.parent_id");

        $result_data = $result_data->from("pages")
            ->join("page_data")->on("pages.id", "=", "page_data." . "page_id")
            ->join("routes")->on("page_data.route_id", "=", "routes.id")
            ->where("routes.nazev_seo", "=", $nazev_seo)
            ->where("routes.zobrazit", "=", 1)
            ->execute()->as_array();

        //die(print_r($result_data[0]));
        if (isset($result_data[0])) {
            $result_data = $result_data[0];
            $result_data["parent_nazev_seo"] = "";
            $result_data["breadcrumbs_end"] = false;



            if (static::$chainable && isset($result_data["parent_id"]) && $result_data["parent_id"] > 0) {
                // nadrazena v tree module
                $parent = DB::select("nazev_seo")
                    ->from("page_data")
                    ->join("routes")->on("page_data.route_id", "=", "routes.id")->on("routes.language_id", "=", db::expr($result_data["language_id"]))
                    ->where("page_data." . "page_id", "=", $result_data["parent_id"])
                    ->execute()->as_array();
                if (isset($parent[0]["nazev_seo"])) {
                    $result_data["parent_nazev_seo"] = $parent[0]["nazev_seo"];
                } else {
                    $result_data["parent_nazev_seo"] = "";

                }

            } else {
                // hlavni modulova
                $result_data["parent_nazev_seo"] = DB::select("routes.nazev_seo")->from("routes")->join("modules")->on("routes.module_id", "=", "modules.id")->where("modules.kod", "=", "page")->where("routes.module_action", "=", "index")->where("routes.language_id", "=", $result_data["language_id"])->where("routes.zobrazit", "=", 1)->where("routes.deleted", "=", 0)->execute()->get("nazev_seo");
                $result_data["breadcrumbs_end"] = true;
            }



            if ($nazev_seo == $result_data["parent_nazev_seo"]) $result_data["parent_nazev_seo"] = "";

            $return = $result_data;

        }

        return $return;


    }
}
?>
