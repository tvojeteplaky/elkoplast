{*  <div id="imgHead">
        <img src="{$media_path}assets/img/stromy.jpg" alt="Stromy">
    </div> *}

    <div class="row">
        <div class="small-12 columns">
            <header>
                <h1>{$item.nadpis}</h1>
            </header>
        </div>
    </div>
<section class="row15" id="products">
		{widget controller="catalog" action="subnav"}
        <div class="large15-11 columns">
        <div class="row">
                <div class="small-12 columns ">
                    {$item.popis}
                </div>
            </div>

            <div class="row large-up-3 medium-up-2 small-up-1 productList ">
                {foreach from=$items item=product key=key name=name}
                    <div class="column">
                    <a href="{$url_base}{$product.nazev_seo}">
                        <div class="product">
                            <div>
                                <img src="{$product.photo}" alt="{$product.nadpis}">
                            </div>
                            <strong>{$product.nadpis}</strong>
                        </div>
                    </a>
                </div>
                {/foreach}
            </div>
            <div class="row desc">
                <div class="small-12 columns ">
                    {$item.uvodni_popis}
                </div>
            </div>
        </div>
    </section>