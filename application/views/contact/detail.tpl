
<article id="contacts">
          <div id="map">
      
          </div>
       {* <div class="row">
            <div class="small-12 columns">
                <header class="headline top">
                    <h2>{translate str="Hlavní kontakty"}</h2>
                </header>
            </div>
        </div>*}
        <div class="row15 hlavniKontakty " data-equalizer data-equalize-on="large">
            <div class="large15-11 columns hlavni" data-equalizer-watch>
                <div class="row">
                   {widget controller="place" action="main"}
                </div>
            </div>
            <div class="large15-4 columns hlavniImg" data-equalizer-watch>
                <img src="{$media_path}assets/img/hlavni.png" alt="{translate str="Hlavní kontakt"}">
            </div>
        </div>
    <div class="row vyhledejtesi sedyrow">
        <div class="small-12 large-5 columns">
            <h2>Vyhledejte obchodního zástupce:</h2>
        </div>
        <div class="small-12 large-7 columns">
            <div class="row">
                <div class="small-12 medium-5 columns">
                    <select id="OborSelectBox" name="obor">
                        <option value="" disabled="disabled">Obor</option>
                        <option value="">Všechny obory</option>
                        {foreach $obory as $obor}
                            <option value="{$obor.id}">{$obor.nazev}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="small-12 medium-5 columns">
                    <select id="ProduktSelectBox" name="product">
                        <option value="" disabled="disabled">Produkt</option>
                        <option value="">Všechny produkty</option>
                        {foreach $produkty as $produkt}
                            <option value="{$produkt.id}">{$produkt.nazev}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="small-12 medium-2 columns">
                    <button class="odeslat" id="sendButton">Hledat</button>
                </div>
            </div>
        </div>
    </div>
    <div id="peopleChangeable">
    {widget action="people" controller="place"}
    </div>
        {*<div class="row">
            <div class="small-12 columns">
                <header class="headline">
                    <h2>{translate str="Kontakty na naše zaměstnance"}</h2>
                </header>
            </div>
        </div>

        <div class="contactAccordion row ">
            <div class="small-12 columns">
                {widget controller="place" action="people"}
            </div>
        </div>*}

        <div class="row">
            <div class="small-12 columns">
                <header class="kontaktniheader">
                    <h2>{translate str="Kontakt na naše zaměstance"}</h2>
                </header>
            </div>

        <form action="" class="dotaz " method="post">
            <div class="row">
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="text" name="contactform[jmeno]" placeholder="{translate str="Jméno a příjmení"}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="tel" name="contactform[telefon]" placeholder="{translate str="Telefonní číslo"}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="email" name="contactform[email]" placeholder="{translate str="Emailová adresa"}">
                            </div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-12 columns">
                                <textarea name="contactform[zprava]" placeholder="{translate str="Text Vaší zprávy"}"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="small-12 columns text-center">
                        <button type="submit" class="button">
                            {translate str="Odeslat zprávu"}
                        </button>
                    </div>
                </div>
            {hana_secured_post action="send" module="contact"}
        </form>
      
    </article>
    {if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}" target="_blank" class="button">Editovat</a>
</div>
{/if}