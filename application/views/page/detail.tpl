

{capture assign="submenu"}
	{if !isset($no_subnav)}{widget controller="page" action="page_subnav"}{/if}
{/capture}
<section class="row{if strlen(trim($submenu))>0}15{/if}">
	{if strlen(trim($submenu))>0}
		<div class="large15-4 columns">
			{$submenu}
		</div>
	{/if}
	<article class="{if strlen(trim($submenu))>0}large15-11{else}small-12{/if} columns">
		<header>
			<h1>{if isset($item.nadpis)}{$item.nadpis}{else}{$item.nazev}{/if}</h1>
		</header>
		<section {if isset($item.nav_class)}id="{$item.nav_class}"{/if} class="">
			{$item.popis}
		</section>
		{if isset($item.files)}
		{foreach from=$item.files item=file key=key name=page_files}
		<a href="{$file.file}" download="download" class="">
			<div class="callout clearfix">
				<img src="{$media_path}assets/img/download.svg" alt="Download" class="float-left">
				<p>{translate str="Přiložený soubor:"} <strong>{$file.nazev}</strong></p>
			</div>
		</a>
		{/foreach}
		{/if}
        {if isset($item.photos)}
		<div class="row">
			<div class="small-12 columns">
				<section class="gallery ">
					<div class="row large-up-3 medium-up-2 small-up-1 productList">
						{foreach from=$item.photos item=photo key=key name=name}
						<div class="column">
							<a href="{$photo.big}" data-lightbox="gallery" title="{$photo.popis}">
								<div class="product">
									<div>
										<img src="{$photo.big}" alt="Placeholder">
									</div>
								</div>
							</a>
						</div>
						{/foreach}
					</div>
				</section>
			</div>
		</div>
		{/if}
	</article>
</section>
{if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}{$item.id}" target="_blank" class="button">Editovat</a>
</div>
{/if}