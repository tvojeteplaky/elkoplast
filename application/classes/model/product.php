<?php defined('SYSPATH') or die('No direct script access.');


class Model_Product extends ORM_Language
{
    protected $_join_on_routes=true;
    
    protected $_product_priceholder=null;

    
    
    //////////// orm vazby, validacni pravidla
    
    protected $_has_many = array(
        //'product_categories' => array('through' => 'product_categories_products'),
        //'product_parameters' => array('through' => 'product_parameters_products'),
        //'downloads'     => array('through' => 'product_downloads_products'),
        //'price_categories' => array('through' => 'price_categories_products'),
        //'product_photos' => array(),
        //'product_files' => array()
    );

    protected $_belongs_to = array(
        //"tax"=>array(),
        //"gallery" => array(),
        //"manufacturer"=>array(),
    ); 

    // Validation rules
	protected $_rules = array(
		'nazev' => array(
			'not_empty'  => NULL,
		),
    	'code' => array(
			'not_empty'  => NULL,
		),
   );
  
      // Validation callbacks
    protected $_callbacks = array(
            'code' => array('code_available')
    );

}
?>
