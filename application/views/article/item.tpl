{foreach from=$items item=article key=key name=name}
<div class="column">
                <a href="{$url_base}{$article.nazev_seo}">
                    <div class="message">
                        <img src="{$article.photo}" alt="{$article.nazev}">
                        {* <div class="date">
                            <time datetime="{$article.date|date_format:"%Y-%m-%d"}">
                                <strong>{$article.date|date_format:"%d"}</strong>
                                <span>{$article.month_name}</span>
                            </time>
                        </div> *}
                        <div class="text">
                            <h3>{$article.nazev}</h3>
                            <p>
                                {$article.popis|truncate:125:"...":true|strip_tags:false}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            {/foreach}