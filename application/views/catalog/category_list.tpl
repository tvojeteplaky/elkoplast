{*
<div id="imgHead">
	<img src="{$media_path}assets/img/stromy.jpg" alt="Stromy">
</div> *}
{* <div class="row"> *}
	{* <div class="small- columns"> *}
		{* <div class="row">
			<div class="small-12 columns">
				<header>
					<h1>{$item.nadpis}</h1>
				</header>
			</div>
		</div> *}
		<div class="row">
			<div class="small-12 columns">
				{widget controller="navigation" action="breadcrumbs"}
			</div>
		</div>
	{* </div> *}
	{* <div class="medium-4 columns text-right">
		{if !empty($item.parent_seo)}<a href="{$url_base}{$item.parent_seo}" class="button buy tiny">&laquo; {translate str="Zpět do nadřazené kategorie"}</a>{/if}
	</div> *}
{* </div> *}
<section class="row15" id="products">
	{widget controller="catalog" action="subnav"}
	<div class="large15-11 columns">
	 	<h2 class="{$color}">{if !empty($color)}<img src="{$media_path}assets/img/{$color}Piktogram.png" alt="{$item.nadpis}">{/if}{$item.nadpis}</h2>
		<div class="row">
			<div class="small-12 columns ">
				{$item.popis}
			</div>
		</div>
		<div class="row large-up-3 medium-up-2 small-up-1 productList {$color} ">
			{foreach from=$items item=product key=key name=name}
			<div class="column">
				<a href="{$url_base}{$product.nazev_seo}">
					<div class="product">
						<div>
							<img src="{if !empty($product.photo_list)}{$product.photo_list}{else}{$product.photo}{/if}" alt="{$product.nadpis}">
						</div>
						<strong>{$product.nadpis}</strong>
					</div>
				</a>
			</div>
			{/foreach}
		</div>
		<div class="row desc">
			<div class="small-12 columns ">
				{$item.uvodni_popis}
			</div>
		</div>
		{widget controller="place" action="category_people" param=$item.id}
		{widget controller="contact" action="show"}
	</div>
</section>
{if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}{$item.id}" target="_blank" class="button">Editovat</a>
</div>
{/if}
