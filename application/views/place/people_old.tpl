<ul class="accordion">
    {foreach from=$places item=place key=key name=places}
    <li class="{if $smarty.foreach.places.first}is-active{/if}">
        <a class="accordion-title" href="#tab-{$smarty.foreach.places.index}">{$place.misto}</a>
        <div class="accordion-content" id="tab-{$smarty.foreach.places.index}">
            <div class="row15 header">
                <div class="large15-6 columns">
                    <div class="row">
                        <div class="small-6 medium-4 large-6 columns">
                            {translate str="Oddělení"}
                        </div>
                        <div class="small-6 medium-4 large-6 end columns">
                            {translate str="Jméno a příjmení"}
                        </div>
                    </div>
                </div>
                <div class="large15-9 columns">
                    <div class="row">
                        <div class="small-6 medium-4 columns">
                            {translate str="Funkce"}
                        </div>
                        <div class="small-6 medium-4 columns">
                            {translate str="Telefonní číslo"}
                        </div>
                        <div class="medium-4 columns">
                            {translate str="Emailová adresa"}
                        </div>
                    </div>
                </div>
            </div>
            {foreach from=$place.people item=person key=key name=name}
            <div class="row15">
                <div class="large15-6 columns">
                    <div class="row">
                        <div class="small-6 medium-4 large-6 columns">
                            {$person.oddeleni}
                        </div>
                        <div class="small-6 medium-4 large-6 end columns">
                            {$person.jmeno}
                        </div>
                    </div>
                </div>
                <div class="large15-9 columns">
                    <div class="row">
                        <div class="small-6 medium-4 columns">
                            {$person.funkce}
                        </div>
                        <div class="small-6 medium-4 columns">
                            {$person.telefon}
                        </div>
                        <div class="medium-4 columns">
                            <a href="mailto:{$person.email}">{$person.email}</a>
                        </div>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
    </li>
    {/foreach}
</ul>
