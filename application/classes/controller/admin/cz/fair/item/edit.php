<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace jednoducheho produktoveho katalogu - list.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2013 Pavel Herink
 */

class Controller_Admin_Cz_Fair_Item_Edit extends Controller_Hana_Edit
{
    protected $with_route=false;
    protected $item_name_property=array("nazev"=>"s názvem");


    public function before() {
        $this->orm=new Model_Fair();

        parent::before();
        $this->image_dir=$this->module_key."/".$this->submodule_key."/";

    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
	    $this->auto_edit_table->row("date")->type("edit")->label("Datum konání")->set();

	    $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku (bílý)")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"jpg","delete_link"=>true))->label("Náhled obrázku")->set();


        $this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();

    }

    protected function _form_action_main_postvalidate($data) {
       parent::_form_action_main_postvalidate($data);

       // vlozim o obrazek
        if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
        {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($this->orm->nazev), true, 'jpg');
        }

    }

    /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false);
    }
}