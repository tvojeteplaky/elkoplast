<article id="contacts" class="ospolecnosti" data-countUp-group>
    <header style="background-image: url('{$item.photo}'), linear-gradient(to right,#f1f2f0,transparent 30%);">
        <div class="row">
            <div class="small-12 large-6 columns">
                {$item.popis}
            </div>
            <div class="small-12 large-6 columns">
                {*<img src="{$item.photo}" alt="{translate str="Join Us"}">*}
            </div>
        </div>
    </header>

    <div class="numbers">
        <div class="row">
            <div class="small-12 large-4 columns">
                <div class="row">
                    <div class="small-12 medium-6 columns text-right">
                        <div class="bignumber" {*id="countUpCislo1" data-countUp="{$web_owner.cislo_1}"*}>{$web_owner.cislo_1}</div>
                    </div>
                    <div class="small-12 medium-6 columns" style="position: relative;">
                        <p>{$web_owner.popis_cislo_1}</p>
			<img src="{$media_path}assets/o-nas-piktogramy/lidi.svg" style="position: absolute; top: 1rem; right: 1rem; width: 40px;" alt="Lidé">
                    </div>
                </div>
            </div>
            <div class="small-12 large-4 columns">
                <div class="row">
                    <div style="padding: 0;" class="small-12 medium-6 columns text-right">
                        <div class="bignumber" {*id="countUpCislo2" data-countUp="{$web_owner.cislo_2}"*}>{$web_owner.cislo_2}</div>
                    </div>
                    <div class="small-12 medium-6 columns" style="position: relative;">
                        <p>{$web_owner.popis_cislo_2}</p>
			<img src="{$media_path}assets/o-nas-piktogramy/planeta.svg" style="position: absolute; top: 1rem; right: 1rem; width: 40px;" alt="Globus">
                    </div>
                </div>
            </div>
            <div class="small-12 large-4 columns">
                <div class="row">
                    <div class="small-12 medium-6 columns text-right">
                        <div class="bignumber" {*id="countUpCislo3" data-countUp="{$web_owner.cislo_3}"*}>{$web_owner.cislo_3}</div>
                    </div>
                    <div class="small-12 medium-6 columns" style="position: relative;">
                        <p>{$web_owner.popis_cislo_3}</p>
			<img src="{$media_path}assets/o-nas-piktogramy/clovek.svg" style="position: absolute; top: 1rem; right: 0; width: 40px;" alt="Zaměstnanci">
                    </div>
                </div>
            </div>
        </div>
    </div>
    {foreach $item.childs as $child}
    {if $child.nav_class != "reference"}
        <div class="{$child.nav_class}" id="{$child.nazev_seo}">
            {if !empty($child.popis) && ($child.nav_class != "doprava" and $child.nav_class != "jsmehrdi") }
                <div class="row text-center">
                    {$child.popis}
                </div>
            {elseif !empty($child.popis) }
                <div class="row">
                    <div class="small-12 medium-6 columns">
                        {$child.popis}
                        {if empty($child.photo_src)}
                        <a href="{$url_base}{$child.nazev_seo}" class="button">{translate str="Číst více"}</a>
                        {/if}
                    </div>
                    {if !empty($child.photo_src)}
                        <div class="small-12 large-6 columns">
                            <img src="{$child.photo_2}" alt="{$item.nazev}">
                        </div>
                    {/if}
                </div>
            {/if}
            {if !empty($child.childs)}
                <div class="row">
                    {foreach $child.childs as $child2}
                        <div class="small-12 large-4 columns" data-href="{$url_base}{$child2.nazev_seo}">
                            <h3>{$child2.nazev}</h3>
                            <img src="{$child2.photo_list}" alt="{$child2.nazev}">
                            {if !empty($child2.popis)}<p>{$child2.popis|strip_tags|strip|trim|truncate:120:"":true}</p>{/if}
                            <a href="{$url_base}{$child2.nazev_seo}" class="button">{translate str="Číst více"}</a>
                        </div>
                    {/foreach}
                </div>

            {/if}

        </div>
	    {if !empty($child.actions)}
            <div class="somegalerry">
                <div class="row small-up-1 medium-up-2 large-up-4">
				    {foreach $child.actions as $action}
					    {capture name="index"}akce_{$action@iteration}{/capture}
                        <div class="column column-block">
                            <a href="#" data-open="{$smarty.capture.index}"  title="{$action.date}">
                                <img src="{$action.photo}" alt="{$action.nazev}" data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title="{$action.date}">
                            </a>
                                <div class="reveal" id="{$smarty.capture.index}" data-reveal>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="text pull-left">
                                        <div class="clearfix">
                                            <img alt="{$action.nazev}" src="{$action.photo}" style="float: left; width: 167px; margin-right: 15px;" />
                                            <h2>{$action.nazev}</h2>

                                            <p><strong>{$action.date}</strong></p>

                                            {$action.popis}
                                        </div>
                                    </div>

                                </div>
                        </div>
				    {/foreach}
                </div>
            </div>
		    {* <div class="row small-up-1 medium-up-2 large-up-4">
				 {foreach $child.actions as $action}
				 <div class="column column-block">
					 <a href="{$url_base}{$action.nazev_seo}">
						 <div class="vystavywrap">
							 <img src="{$action.photo}" alt="{$action.nazev}">
							 <p>{$action.nazev} <br>
								 {$action.date|date:"cs"} {$action.place}</p>
						 </div>
					 </a>
				 </div>
				 {/foreach}
			 </div>*}

        {elseif !empty($child.photos)}
            <div class="somegalerry">
                <div class="row small-up-1 medium-up-2 large-up-4">
                    {foreach $child.photos as $photo}
                            {capture name="index"}akce_{$photo@iteration}{/capture}
                        <div class="column column-block">
                            <a href="{if empty($child[$smarty.capture.index])}{$photo.big}{else}#{/if}" {if empty($child[$smarty.capture.index])}data-lightbox="gallery-{$child.id}"{else}data-open="{$smarty.capture.index}"{/if} >
                            <img src="{$photo.big}" alt="{$photo.nazev}">
                            </a>
                            {if !empty($child[$smarty.capture.index])}
                            <div class="reveal" id="{$smarty.capture.index}" data-reveal>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                {$child[$smarty.capture.index]}
                            </div>
                            {/if}
                        </div>
                    {/foreach}
                </div>
            </div>
        {/if}
        {else}
        {assign var="reference" value=$child}
        {/if}
    {/foreach}
    <div class="milniky" id="milniky">
        <h2 class="text-center">{translate str="Milníky"}</h2>
        <div class="text-center">
            <div class="horizontalInfinity">
                      <div class="viewfinder">
                          <div class="picture">
                              <div class="timeLine">
                              {foreach $events as $event}
                                  <div class="event">
                                      <div class="line">
                                          <div class="dot">
                                          </div>
                                      </div>
                                      <div>
                                          <h2>{$event.year}</h2>
                                          <p>
                                             {$event.popis}
                                          </p>
                                      </div>
                                  </div>
                                  
                              {/foreach}
                              </div>
                          </div>
                          <div class="controls">
                              <div class="back"></div>
                              <div class="forward"></div>
                          </div>
                      </div>
                  </div>
        </div>
    </div>
    {*<div id="map2"></div>*}

    <div class="certifikace">
        <h2 class="text-center">{translate str="Společnost ELKOPLAST CZ je držitelem certifikátů kvality:"}</h2>
        <div class="row small-up-1 medium-up-{$item.photos|count}">
            {foreach $item.photos as $photo}
                <div class="column column-block text-{if $photo@first}right{else}left{/if}">
                    <img src="{$photo.cert}" alt="{$photo.nazev}">
                </div>
            {/foreach}
        </div>
    </div>
    {if isset($reference)}
        <div class="certifikace" id={$reference.nazev_seo}>
            <h2 class="text-center">{translate str="Reference:"}</h2>
            <div class="row small-up-2 medium-up-4 large-up-6">
                {foreach $reference.photos as $photo}
                    <div class="column column-block text-center">
                        <img src="{$photo.cert}" alt="{$photo.nazev}" >
                    </div>
                {/foreach}
            </div>
        </div>

    {/if}
</article>
{if isset($edit) && !empty($edit)}
<div class="adminEditButton">
<a href="{$edit}{$item.id}" target="_blank" class="button">Editovat</a>
</div>
{/if}
