{foreach from=$places item=place key=key name=name}
<div class="medium-4 columns" id="psc{$place.psc|replace:' ':''}">
    <h2>{$place.nazev}</h2>
    <em>{$place.misto}  <br>({$place.funkce})</em>
    <strong>
        {$place.ulice},
        <br>
        {$place.psc} {$place.mesto}
    </strong>
    <div>
        <span><img src="{$media_path}assets/img/tel.svg" alt="{translate str="Telefon"}" class="mobil">{$place.tel}</span>
        <span class="mail">{if !empty($place.email)}<span class="at">@</span><span class="mailText">{$place.email}</span>{/if}</span>
        {if !empty($place.schranka)}<span><img src="{$media_path}assets/img/email.svg" alt="Datová schránka" class="email"><b>{$place.schranka}</b></span>{/if}
    </div>
</div>
{/foreach}

