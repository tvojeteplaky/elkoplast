<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 */
class Controller_Catalog extends Controller
{
    private $colors = array(
        "odpadove-hospodarstvi" => "odpad",
        2 => "odpad",
        "dum-a-zahrada" => "dumAZahrada",
        3 => "dumAZahrada",
        "hospodareni-s-kapalinami" => "kapaliny", 
        4 => "kapaliny",
        "zimni-udrzba" => "zimniUdrzba",
        5 => "zimniUdrzba",
        "skladovani" => "skladovani",
        6 => "skladovani"
        );
    /**
     * Metoda generujici seznam clanku.
     */
    public function action_index($page=1)
    {
        $template = new View("catalog/category_list");
        $route_id = $this->application_context->get_route_id();
        $page_orm  = Service_Page::get_page_by_route_id($route_id);

        $template->items = Service_Catalog_Category::get_categories_by_parent_id(0, $this->application_context->get_actual_language_id(),1);
        $template->item = $page_orm;
        $template->color = "";
        $this->request->response=$template->render();
    }

    public function action_category()
    {
        $route_id=$this->application_context->get_actual_route();
        $category = Service_Catalog_Category::get_product_category_by_route_id($route_id);
       // $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
       // end($sel_links);
        $base_cat = Service_Catalog_Category::get_base_category($category["id"]);
        if($category["parent_id"] == 0) {
            $color = $this->colors[$category["id"]];
        } else if(array_key_exists($category["parent_id"],  $this->colors)) { 
            $color = $this->colors[$category["parent_id"]];
        } else {
            
        }
        $products = Service_Catalog_Category::get_categories_by_parent_id($category["id"], $this->application_context->get_actual_language_id(),1);
        $template = new View("catalog/category_list");
        $template->item = $category;
        $template->items = $products;
        $template->color = $this->colors[$base_cat->id];
        if(Auth::instance()->logged_in(array('login', 'admin'))) {
            $template->edit = url::base()."admin/cz/catalog/category/edit/".$this->application_context->get_actual_language_id()."/";
        }
        //$template->prev = current($sel_links);
        $this->request->response=$template->render();
    }

    /**
     * Metoda generujici seznam clanku - uvodka.
     */
    public function action_widget()
    {
        $template=new View("catalog/widget");
        $categories = Service_Catalog_Category::get_categories_by_parent_id(0, $this->application_context->get_actual_language_id(), 1, 6);
        $template->categories = $categories;
        $template->colors = $this->colors;
        $this->request->response = $template->render();
    }


    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id=$this->application_context->get_actual_route();
        $template=new View("catalog/detail");
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        array_pop($sel_links);
        $category=array_pop($sel_links);
        //$template->link_back=$category["nazev_seo"];
        $item=Service_Catalog::get_catalog_item_by_route_id($route_id);
        $template->main_photo = array_shift($item["photos"]);
        $template->item=$item;

        
        $base_cat = Service_Catalog_Category::get_base_category($item["product_category_id"]);
        $template->color = $this->colors[$base_cat->id];
        if(Auth::instance()->logged_in(array('login', 'admin'))) {
            $template->edit = url::base()."admin/cz/catalog/item/edit/".$this->application_context->get_actual_language_id()."/";
        }
        $this->request->response=$template->render();
    }

    public function action_subnav() {

          $template=new View("catalog/subnav");
          $template->links = Service_Catalog_Category::get_categories_by_parent_id(0, $this->application_context->get_actual_language_id(),3,0,false);
          $template->sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();

          $this->request->response = $template->render();

    }

    public function action_main_subnav($nazev_seo)
    {
        $subnav=new View("catalog_top_subnav");
        $links     = Service_catalog::get_navigation($this->application_context->get_actual_language_id(),1,1);
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $subnav->links = $links;
        $subnav->sel_links = $sel_links;
        $subnav->prev = prev(end($sel_links));
        $this->request->response=$subnav->render();
    }

    public function action_homepage_widget()
    {
        $template = new View("catalog/homepage_widget");
        $products = Service_Catalog_Category::get_for_homepage();

        $template->products = $products;
        $this->request->response = $template->render();
    }

}

?>
