<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Administrace stranek - edit.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Admin_Cz_Timeline_Item_Edit extends Controller_Hana_Edit
{
    protected $item_name_property = array("nazev" => "s názvem");


    public function before()
    {
        $this->orm = new Model_Timeline();

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
         $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
         $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
            $this->auto_edit_table->row("year")->type("edit")->label("Rok")->set();
        $this->auto_edit_table->row("popis")->type("editor")->label("Hlavní text")->set();
    }

}
