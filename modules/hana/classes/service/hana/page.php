<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Servisa pro obsluhu statickych stranek.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Hana_Page extends Service_Hana_Module_Base
{
    public static $photos_resources_dir="media/photos/";
    public static $files_resources_dir="media/files/";
    public static $navigation_module="page";
    protected static $chainable=true;
    protected static $thumbs = array("cert" =>"cert",'small'=>'t1','big'=>'ad');

    /**
     * Nacte stranku dle route_id
     * @param int $id
     * @return orm
     */
    public static function get_page_by_route_id($id)
    {
        $page_orm= orm::factory("page")->where("route_id","=",$id)->find();
        if($page_orm->direct_to_sublink)
        {
            $language_id=  Hana_Application::instance()->get_actual_language_id();
            //throw new Kohana_Exception("Požadovaná stránka nebyla nalezena", array(), "404");
            $first_subpage_seo=DB::select("routes.nazev_seo")
                ->from("pages")
                ->join("page_data")->on("pages.id","=","page_data.page_id")
                ->join("routes")->on("page_data.route_id","=","routes.id")
                ->where("pages.parent_id","=",db::expr($page_orm->id))
                ->where("routes.language_id","=",DB::expr($language_id))
                ->where("routes.zobrazit","=",DB::expr(1))
                ->order_by("poradi")
                ->limit(1)
                ->execute()->get("nazev_seo");
            Request::instance()->redirect(url::base().$first_subpage_seo);
        }

        $result_data=$page_orm->as_array();

        // cesta k obrazku
        $dirname=self::$photos_resources_dir."page/item/images-".$page_orm->id."/";

        if($page_orm->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$page_orm->photo_src."-t1.jpg"))
        {
            $result_data["photo"]=url::base().$dirname.$page_orm->photo_src."-t1.jpg";
            $result_data["photo_2"]=url::base().$dirname.$page_orm->photo_src."-t2.png";
            $result_data["photo_list"]=url::base().$dirname.$page_orm->photo_src."-list.jpg";
        } else {
            $result_data["photo"]="";
            $result_data["photo_2"]="";
            $result_data["photo_list"]="";
        }

        $result_data['photos'] = array();
        $photos_orm = $page_orm->page_photos
                            ->where('zobrazit','=',1)
                            ->where('language_id','=',$page_orm->language_id)
                            ->order_by('poradi',self::$order_direction)
                            ->find_all();

        foreach ($photos_orm as $photo) {
            $result_data['photos'][$photo->id] = $photo->as_array();
            foreach (self::$thumbs as $key => $thumb) {

                $dirname=self::$photos_resources_dir.self::$navigation_module."/item/gallery/images-". $page_orm->id.'/'.$photo->photo_src.'-'.$thumb.'.';

                if($photo->photo_src && file_exists(str_replace('\\', '/', DOCROOT).$dirname."jpg")){

                    $result_data['photos'][$photo->id][$key] = $dirname."jpg";
                } else if($photo->photo_src && file_exists(str_replace('\\', '/', DOCROOT).$dirname."png")) {
                    $result_data['photos'][$photo->id][$key] = $dirname."png";
                } else {
                    $result_data['photos'][$photo->id][$key] = "";
                }
            }
        }


        //        // vylistovani souburu
        $files = $page_orm->page_files->where("page_file_data.language_id", "=", $result_data["language_id"])->where("page_files.zobrazit", "=", 1)->find_all();
        $filedirname = self::$files_resources_dir . "page/item/files-" . $page_orm->id . "/";
        $files_array = array();
        $x = 1;
        foreach ($files as $file) {
          if ($file->file_src && file_exists(str_replace('\\', '/', DOCROOT) . $filedirname . $file->file_src . "." . $file->ext)) {
            $files_array[$x]["file"] = url::base() . $filedirname . $file->file_src . "." . $file->ext;
            $files_array[$x]["nazev"] = $file->nazev;
            $files_array[$x]["ext"] = $file->ext;
            $files_array[$x]["file_thumb"] = url::base() . "media/admin/img/icons/" . $file->ext . ".png";
            $x++;
          }
        }
        $result_data["files"] = $files_array;


        return $result_data;
    }

    /**
     * Najde podřízené stránky.
     * @param type $page_id
     * @return orm
     */
    public static function get_page_index($page_id)
    {
        $language_id=  Hana_Application::instance()->get_actual_language_id();
        $index_pages=DB::select("pages.photo_src")->select("pages.id")->select("page_data.nazev")->select("page_data.uvodni_popis")->select("page_data.akce_text")->select("routes.nazev_seo")
                ->from("pages")
                ->join("page_data")->on("pages.id","=","page_data.page_id")
                ->join("routes")->on("page_data.route_id","=","routes.id")
                ->where("pages.parent_id","=",db::expr($page_id))
                ->where("routes.language_id","=",DB::expr($language_id))
                ->where("routes.zobrazit","=",DB::expr(1))
                ->order_by("poradi")
                ->execute();

        $index_pages=$index_pages->as_array();
        $result_data=array();

        foreach ($index_pages as $page)
        {
            $result_data[$page["nazev"]]["nazev"]=$page["nazev"];
            $result_data[$page["nazev"]]["nazev_seo"]=$page["nazev_seo"];
            $result_data[$page["nazev"]]["uvodni_popis"]=$page["uvodni_popis"];
            $result_data[$page["nazev"]]["akce_text"]=$page["akce_text"];

            $dirname=self::$photos_resources_dir."page/item/images-".$page["id"]."/";
            if($page["photo_src"] && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$page["photo_src"]."-t2.jpg"))
            {
                $result_data[$page["nazev"]]["photo_detail"]=url::base().$dirname.$page["photo_src"]."-t2.jpg";
            }
        }


        return $result_data;
    }


    /**
     *
     * @param type $code
     * @param type $language_id
     * @return type
     */
    public static function get_static_content_by_code($code, $language_id=1)
    {
        return orm::factory("static_content")->where("kod","=",$code)->where("language_id","=",$language_id)->find();
    }

    public static function search_config()
    {
        return array(

                  "title"=>"Výsledky ve stránkách",
                  "display_title"=>"page_data.nazev",
                  "display_text"=>"page_data.popis",
                  "display_photo" => "t3.jpg",
                  "display_photo_src" => self::$photos_resources_dir."page/item/",
                  "search_columns"=>array("page_data.nazev", "page_data.popis", "page_data.uvodni_popis"),
//                  "display_category_title"=>"product_category_data.nazev",
//                  "display_category_text"=>"product_category_data.uvodni_popis",
//                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")

        );
    }

}
?>
