<article id="contacts" class="ospolecnosti">

	<div class="certifikace">
		<h2 class="text-center">{$page.nadpis}</h2>
	<div class="row small-up-2 medium-up-4 large-up-6">
		{foreach $page.photos as $photo}
			<div class="column column-block text-center">
				<img src="{$photo.cert}" alt="{$photo.nazev}" >
			</div>
		{/foreach}
	</div>
</div>
</article>