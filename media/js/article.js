$(document).ready(function() {

	localStorage.offset = 3;
	$("#loadNext").click(function() {
		$.get("?"+$(this).data("url"), "offset="+localStorage.offset, function (data) {
			data = JSON.parse(data);
			$("#newsList").append(data.main_content);
			localStorage.offset = localStorage.offset*1+3;

		});
	});

	if(document.messages) {
		$("#message").foundation('open');
		setTimeout(function(){
			$("#message").foundation('close');
		},3000)
	}
});