<?php defined('SYSPATH') or die('No direct script access.');

class Model_Place extends ORM_Language
{

	protected $_join_on_routes=false;

	// Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty'  => NULL,
        ),
    );

   protected $_has_many = array(
   	"place_people" => array(),
   	);
}