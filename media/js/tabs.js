$(function() {
	var tabs = $(".accordion");
	if(tabs.length > 0) {
		tabsHidder(tabs,0);

		tabs.on("click","a.accordion-title", function(ev) {
			ev.preventDefault();
			tabsHidder(tabs,300,this);
		});
	}
});

function tabsHidder(tabs,time,tab) {
	if(typeof tab != "undefined" && $(tab).parent().hasClass("is-active")) {
		$(tab).parent().removeClass("is-active").find(".accordion-content").hide(time);

	} else if(typeof tab != "undefined") {
		tabs.find("li").removeClass("is-active");
		$(tab).parent().addClass("is-active");
		
	}

		tabs.find(".accordion-content").hide();
		tabs.find(".is-active .accordion-content").show(time);
}